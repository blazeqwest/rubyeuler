=begin
 10001st prime
 Problem 7
 By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

 What is the 10 001st prime number?
=end

def prime(x)
  i = 0
  z = 0
  if x == 0
    return 0
  else  
    while z < x do
      i += 1
      z += 1 if is_simple(i)
      #puts "iteration #{i} number #{z}"
    end
   end
  return i
end

def is_simple(num)
  result = 0
  num = num / 2
  (1..num).each {
    |n| result += 1 if num % n == 0
    break if result >= 3
    }
  return result == 2
end
#puts prime(1)
#puts prime(2)
#puts prime(3)
#puts prime(4)
#puts prime(5)
#puts prime(6)
#puts prime(7)
puts prime(10001)
