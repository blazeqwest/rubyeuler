# Largest palindrome product
# Problem 4
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
# 
# Find the largest palindrome made from the product of two 3-digit numbers.
def pal (max)
  i = 1
  j = 1
  result = [1,0,0]
  mid = 1
  for i in 1..max do
    for j in 1..max do
      mid = i * j
      if pal_check(mid) && mid > result[0]
        result[0] = mid
        result[1] = i
        result[2] = j
      end
    end
  end  
  return result
end

def pal_check(num)
  reverted = 0
  eta = num
  while num >= 1 do
    reverted *= 10
    reverted += num % 10
    num = (num / 10) - (num % 10) / 10
  end
    if eta == reverted
      return true
    else 
      return false
    end
end


def test_pal
  if pal(99) == [9009, 91, 99] || pal(99) == [9009, 99, 91]
    puts "Test 1 passed, palindrom of product of 91 and 99"
    puts "The largest palindrome made from the product of two 3-digit numbers is #{pal(999)}" 
  elsif
    puts "Palindrom test failed - #{pal(9)} instead of 9009"
  end
end

def test_reverse
  if pal_check(101) && !pal_check(134) && !pal_check(217)
    puts "Pal_check function passed!" 
  else
    puts "Pal_check function test failed!"
  end
end

test_pal
test_reverse