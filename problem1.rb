#Multiples of 3 and 5
#Problem 1
#If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
#Find the sum of all the multiples of 3 or 5 below 1000.

def p1 (m1,m2,max)
  result = 0
  for i in 3...max do
    if i % m1 == 0 && i % m2 == 0
      result += i
    elsif  i % m1 == 0 
      result += i  
    elsif  i % m2 == 0 
      result += i
    end   
  end
  return result
end



def test
  if p1(3,5,10) == 23
    puts "Test passed, the sum of these multiples is 23"
    puts "The sum of all the multiples of 3 or 5 below 1000 is #{p1(3,5,1000)}" 
  elsif
    puts "Test failed, actual result is #{p1(3,5,10)}"
  end
end

test