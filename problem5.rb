# Problem 5
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

def p5 (max)
  i = 2
    # если число не делится на все без остатка, то повышаем число на 1 до тех пор, пока не число не будет делиться на все без остатка
    until rem(i, max) do i += 1
      result = i
    end
  return result
end

def rem (num, max)
  result = true
  for i in 1..max
    if num % i != 0
      result = false
      break  
    end
  end
  return result
end


def test
  if p5(10) == 2520
    puts "Test passed, 2520 can be devided by numbers from 1 to 10 without remainder"
    puts "\n"
    puts "ok..."
    puts "\n"
    puts "The smallest positive number that is evenly divisible by all of the numbers from 1 to 20 is #{p5(20)}" 
  elsif
    puts "Test failed: #{p5(10)} instead of 2520"
  end
end

test
