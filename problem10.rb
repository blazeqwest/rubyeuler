=begin
 Problem 10
 
 The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

 Find the sum of all the primes below two million.
Benchmark results (total time elapsed): 963.019824 
=end
require 'benchmark'
# решето Эратосфена
def dev(array, num)
  array.each do |i|
    return false if num % i == 0
  end
  return true 
end

def summ(top)
  a = [2]
  sum = 0
  i = 3
  until i >= top do
    a.push(i) if dev(a,i)
    i += 1
  end
  a.each {|v| sum += v}
  return sum
end 

puts Benchmark.measure {puts summ(2_000_000)}
