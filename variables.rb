class Sample
  $number_of_descendants = 0
  def initialize
    $number_of_descendants += 1
  end
  def count
    puts "Number of Sample objects is #{$number_of_descendants}"
  end
end

sample1 = Sample.new
sample2 = Sample.new
puts $number_of_descendants

(1..5).each do |n|
  puts n
end
