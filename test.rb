require 'open-uri'

# Add your code below!
website = open("http://placekitten.com/")

puts website.status
puts "\n"

my_hash = { 
  "name" => "Eric",
  "age" => 26,
  "hungry?" => true
}

puts my_hash["name"]
puts my_hash["age"]
puts my_hash["hungry?"]
puts "\n"

pets = Hash.new
pets ["cat"] = "mi"
puts pets["cat"]