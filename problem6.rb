# Problem 6
# The sum of the squares of the first ten natural numbers is,

# 1**2 + 2**2 + ... + 10*2 = 385
# The square of the sum of the first ten natural numbers is,

# (1 + 2 + ... + 10)**2 = 55**2 = 3025
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.

# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
def s1(max)
  result = 0
    for i in 1..max do
      result += i * i 
    end
  return result
end

def s2(max) 
   result = 0
    for i in 1..max do
      result += i
    end  
   return result *= result  
end

def test
  if s1(10) == 385 && s2(10) == 3025
    puts "Test 1&2 passed."
    puts "The difference between the sum of the squares of the first one hundred natural numbers and the square of the sum is #{s2(100) - s1(100)}"  
  end
end

test
