=begin
 2**15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

 What is the sum of the digits of the number 2**1000? 
=end
def sum(p)
  res = 0
  num = 2**p
  num.to_s.split("").each do |i|
    res += i.to_i
  end
  return res
end

puts sum(1000)
